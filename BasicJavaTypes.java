package assignment2;

public class BasicJavaTypes {

	public static void main(String[] args) {
		int i=12345;
		byte b=100;
		short s=123;
		float f=1.43f;
		long l=1234567890;
		double d=1234.5678;
		char c='a';
		boolean bl=false;
		System.out.println("int Value = "+ i);
		System.out.println("byte Value = "+ b);
		System.out.println("short Value = "+ s);
		System.out.println("float Value = "+ f);
		System.out.println("long Value = "+ l);
		System.out.println("double Value = "+ d);
		System.out.println("boolean Value = "+ bl);
		System.out.println("char Value = "+ c);

	}

}
